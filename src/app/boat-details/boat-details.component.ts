import { Component, OnInit } from '@angular/core';
import {Boat} from "../../classes/boat";
import {ActivatedRoute, Router} from "@angular/router";
import {BoatService} from "../../services/boat.service";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-boat-details',
  templateUrl: './boat-details.component.html',
  styleUrls: ['./boat-details.component.css']
})
export class BoatDetailsComponent implements OnInit {

  // @ts-ignore
  idPath: number;
  // @ts-ignore
  boat: Boat;

  constructor(private activatedRoute:ActivatedRoute,private authService: AuthenticationService,  private boatService : BoatService, private router: Router) { }

  ngOnInit(): void {
    if(this.authService.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.authService.jwtToken = null;
      this.authService.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.authService.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        let jwtToken = resp.accesstoken;
        // @ts-ignore
        let refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.authService.saveToken(jwtToken);
        // @ts-ignore
        this.authService.saveRefreshToken(refreshToken);

        this.idPath = this.activatedRoute.snapshot.params['id'];
        this.boatService.getBoatById(this.idPath).subscribe( data => {
          this.boat = data;
        }, error => console.log(error));
      })
    }
    else {
      this.idPath = this.activatedRoute.snapshot.params['id'];
      this.boatService.getBoatById(this.idPath).subscribe( data => {
        this.boat = data;
      }, error => console.log(error));
    }
  }

}
