import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BoatsComponent} from "./boats/boats.component";
import {AddBoatComponent} from "./add-boat/add-boat.component";
import {UpdateBoatComponent} from "./update-boat/update-boat.component";
import {BoatDetailsComponent} from "./boat-details/boat-details.component";
import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";

const routes: Routes = [
  {path: 'boats', component: BoatsComponent},
  {path: 'add-boat', component: AddBoatComponent},
  {path: 'update-boat/:id', component: UpdateBoatComponent},
  {path: 'boat-details/:id', component: BoatDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegistrationComponent},
  {path: '', redirectTo : 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
