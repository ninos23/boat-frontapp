import { Component, OnInit } from '@angular/core';
import {Boat} from "../../classes/boat";
import {BoatService} from "../../services/boat.service";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-add-boat',
  templateUrl: './add-boat.component.html',
  styleUrls: ['./add-boat.component.css']
})
export class AddBoatComponent implements OnInit {

  boat: Boat = new Boat();

  constructor(private boatService : BoatService, private authService : AuthenticationService, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.saveBoat();
  }

  saveBoat() {

    if(this.authService.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.authService.jwtToken = null;
      this.authService.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.authService.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        let jwtToken = resp.accesstoken;
        // @ts-ignore
        let refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.authService.saveToken(jwtToken);
        // @ts-ignore
        this.authService.saveRefreshToken(refreshToken);

        this.boatService.addBoat(this.boat).subscribe(
          data => {
            console.log(this.boat)
            this.goToBoatsList();
          },
          error => console.log(error)
        );
      })
    }
    else {
      this.boatService.addBoat(this.boat).subscribe(
        data => {
          console.log(this.boat)
          this.goToBoatsList();
        },
        error => console.log(error)
      );
    }


  }

  goToBoatsList() {
    this.router.navigate(["boats"]);
  }
}
