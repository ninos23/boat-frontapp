import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private authService : AuthenticationService, private router : Router) { }

  ngOnInit(): void {
  }

  save(user: any) {
    this.authService.register(user).subscribe(resp => {
      this.router.navigateByUrl('/login');
    },error => console.log(error));
  }
}
