import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mode : number = 0;


  constructor(private authService : AuthenticationService, private router : Router) { }

  ngOnInit(): void {
    }

  onLogin(user: any) {

    this.authService.login(user).subscribe(resp => {
      // @ts-ignore
      this.authService.saveToken(resp.body.accesstoken);
      // @ts-ignore
      this.authService.saveRefreshToken(resp.body.refreshtoken);
      this.router.navigateByUrl('/boats');
    },error => this.mode=1);
  }

  onRegister() {
    this.router.navigateByUrl('/register');
  }
}
