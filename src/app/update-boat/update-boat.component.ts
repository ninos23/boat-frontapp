import { Component, OnInit } from '@angular/core';
import {Boat} from "../../classes/boat";
import {BoatService} from "../../services/boat.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-update-boat',
  templateUrl: './update-boat.component.html',
  styleUrls: ['./update-boat.component.css']
})
export class UpdateBoatComponent implements OnInit {

  boat : Boat = new Boat();
  // @ts-ignore
  id: number;

  constructor(private boatService : BoatService,
              private activatedRoute : ActivatedRoute,
              private authService : AuthenticationService,
              private router : Router) {
  }

  ngOnInit(): void {
    if(this.authService.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.authService.jwtToken = null;
      this.authService.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.authService.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        let jwtToken = resp.accesstoken;
        // @ts-ignore
        let refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.authService.saveToken(jwtToken);
        // @ts-ignore
        this.authService.saveRefreshToken(refreshToken);

        this.id = this.activatedRoute.snapshot.params['id'];
        this.boatService.getBoatById(this.id).subscribe(data => {
          this.boat = data;
        }, error => console.log(error));
      })
    }
    else {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.boatService.getBoatById(this.id).subscribe(data => {
        this.boat = data;
      }, error => console.log(error));
    }


  }

  onSubmit() {

    if(this.authService.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.authService.jwtToken = null;
      this.authService.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.authService.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        let jwtToken = resp.accesstoken;
        // @ts-ignore
        let refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.authService.saveToken(jwtToken);
        // @ts-ignore
        this.authService.saveRefreshToken(refreshToken);

        this.boatService.updateBoat(this.id, this.boat).subscribe(data => {
          this.goToBoatsList();
        }, error => console.log(error));
      })
    }
    else {
      this.boatService.updateBoat(this.id, this.boat).subscribe(data => {
        this.goToBoatsList();
      }, error => console.log(error));
    }


  }

  goToBoatsList() {
    this.router.navigate(["boats"]);
  }
}
