import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoatsComponent } from './boats/boats.component';
import {FormsModule, NgForm} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { AddBoatComponent } from './add-boat/add-boat.component';
import { UpdateBoatComponent } from './update-boat/update-boat.component';
import {ConfirmationPopoverModule} from "angular-confirmation-popover";
import { BoatDetailsComponent } from './boat-details/boat-details.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';

@NgModule({
  declarations: [
    AppComponent,
    BoatsComponent,
    AddBoatComponent,
    UpdateBoatComponent,
    BoatDetailsComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ConfirmationPopoverModule.forRoot({
      focusButton: 'confirm',
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
