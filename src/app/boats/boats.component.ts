import { Component, OnInit } from '@angular/core';
import {BoatService} from "../../services/boat.service";
import {Router} from "@angular/router";
import {Boat} from "../../classes/boat";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
  selector: 'app-boats',
  templateUrl: './boats.component.html',
  styleUrls: ['./boats.component.css']
})
export class BoatsComponent implements OnInit {

  boatList: any;
  boat: Boat = new Boat();

  constructor(public authService : AuthenticationService, private boatService: BoatService, private router : Router) { }

  ngOnInit(): void {
    this.getBoats();
  }

  private getBoats() {

    if(this.authService.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.authService.jwtToken = null;
      this.authService.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.authService.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        let jwtToken = resp.accesstoken;
        // @ts-ignore
        let refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.authService.saveToken(jwtToken);
        // @ts-ignore
        this.authService.saveRefreshToken(refreshToken);

        this.boatService.getBoats().subscribe(data => {
          this.boatList = data;
        })
      })
    }
    else {
      this.boatService.getBoats().subscribe(data => {
        this.boatList = data;
      })
    }
  }

  updateBoat(id: number) {
    this.router.navigate(['update-boat', id]);
  }

  deleteBoat(id: number) {
    if(this.authService.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.authService.jwtToken = null;
      this.authService.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.authService.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        let jwtToken = resp.accesstoken;
        // @ts-ignore
        let refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.authService.saveToken(jwtToken);
        // @ts-ignore
        this.authService.saveRefreshToken(refreshToken);

        this.boatService.deleteBoat(id).subscribe(data => {
          console.log(data)
          this.getBoats();
        })
      })
    }
    else {
      this.boatService.deleteBoat(id).subscribe(data => {
        console.log(data)
        this.getBoats();
      })
    }
  }

  boatDetails(id: number) {
    this.router.navigate(['boat-details', id]);
  }
}
