import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Boat} from "../classes/boat";
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class BoatService {

  private jwt = null;
  private baseURL = "http://localhost:8080/boats";
  constructor(private httpClient : HttpClient, private authenticationService : AuthenticationService) {
    this.jwt = this.authenticationService.jwtToken;
  }

  getBoats(): Observable<Boat[]> {
    if(this.jwt == null) {
      // @ts-ignore
      this.jwt = this.authenticationService.loadToken();
    }

     // @ts-ignore
    return this.httpClient.get<Boat[]>(this.baseURL, this.authenticationService.getHeaders());
  }

  addBoat(boat : Boat): Observable<Object> {
    return this.httpClient.post(this.baseURL, boat, this.authenticationService.getHeaders());
  }

  getBoatById(id : number): Observable<Boat> {
    return this.httpClient.get<Boat>(this.baseURL + '/' + id, this.authenticationService.getHeaders());
  }

  updateBoat(id : number, boat : Boat): Observable<Object> {
    return this.httpClient.patch(this.baseURL + '/' + id, boat, this.authenticationService.getHeaders());
  }

  deleteBoat(id : number): Observable<Object> {
    return this.httpClient.delete(this.baseURL + '/' + id, this.authenticationService.getHeaders());
  }



}
