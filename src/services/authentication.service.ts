import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private host: string = "http://localhost:8080";
  jwtToken = null;
  refreshToken = null;
  // @ts-ignore
  private roles: Array<any>;
  private isExpired :boolean = true;

  constructor(private http : HttpClient, private router : Router) { }

  getHeaders(): {} {
    if (this.jwtToken == null) {
      this.loadToken();
    }
    // @ts-ignore
    let headers = new HttpHeaders({'Authorization': this.jwtToken});
    headers.set("Cache-control", "no-cache");
    headers.set("Cache-control", "no-store");
    headers.set("Expires", "0");
    headers.set("Pragma", "no-cache");
    return {headers};
  }

  login(user : any) {
    return this.http.post(this.host + "/login", user , {observe: "response"});
  }

  logout() {
    this.jwtToken = null;
    localStorage.removeItem('token');
    localStorage.removeItem('refreshtoken');
    this.router.navigateByUrl("/login");
  }

  saveToken(jwt: string) {
    // @ts-ignore
    this.jwtToken = jwt;
    //this.jwtToken = jwt + "ji";
    // encode base64 for mor security
    //localStorage.setItem('token', btoa(JSON.stringify(this.jwtToken));
    localStorage.setItem('token', jwt);
    let jwtHelper = new JwtHelperService();
    // @ts-ignore
    this.roles = jwtHelper.decodeToken(this.jwtToken).roles;
  }

  saveRefreshToken(jwt: string) {
    // @ts-ignore
    this.refreshToken = jwt;
    //this.jwtToken = jwt + "ji";
    // encode base64
    //localStorage.setItem('token', btoa(JSON.stringify(this.jwtToken));
    localStorage.setItem('refreshtoken', jwt);
  }

  loadToken() {
    // @ts-ignore
    this.jwtToken = localStorage.getItem("token");
    // decode base64
    //this.jwtToken = JSON.parse(atob(this.jwtToken));
    //this.jwtToken = this.jwtToken.substring(0, this.jwtToken.length -2);
  }

  loadRefreshToken() {
    // @ts-ignore
    this.refreshToken = localStorage.getItem("refreshtoken");
    // decode base64
    //this.jwtToken = JSON.parse(atob(this.jwtToken));
    //this.jwtToken = this.jwtToken.substring(0, this.jwtToken.length -2);
  }

  register(body: any) {
    return this.http.post(this.host + "/register", body);
  }

  isAdmin() {
    for (let r of this.roles) {
      if (r == "ADMIN") {
        return true;
      }
    }
    return false;
  }

  getRefreshToken(refreshtokenParam : any) : Observable<any>{
    // @ts-ignore
    return this.http.get(this.host + "/refreshToken" , {headers: new HttpHeaders({"Authorization": refreshtokenParam})});
  }

  isJwtExpired() {
    let jwtHelper = new JwtHelperService();
    // @ts-ignore
    this.isExpired = jwtHelper.isTokenExpired(this.jwtToken);
    return this.isExpired
  }

  checkReloadTokenExpiration() {
    if(this.isJwtExpired()) {
      let refreshtokenLocal = localStorage.getItem("refreshtoken");
      this.jwtToken = null;
      this.refreshToken = null;
      localStorage.removeItem('token');
      localStorage.removeItem('refreshtoken');
      this.getRefreshToken(refreshtokenLocal).subscribe(resp => {

        // @ts-ignore
        this.jwtToken = resp.accesstoken;
        // @ts-ignore
        this.refreshToken = resp.refreshtoken;
        // @ts-ignore
        this.saveToken(this.jwtToken);

        // @ts-ignore
        this.saveRefreshToken(this.refreshToken);
      })
    }
  }

}
